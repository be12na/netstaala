@extends('layouts.app-mitra')

@section('content')
    <div class="d-block w-100 table-responsive border">
        <table class="table table-sm table-nowrap table-hover table-tree border-bottom mb-2" id="table"
            data-child-url="{{ route('mitra.myMember.structure.index') }}">
            <thead class="bg-gradient-brighten bg-white small">
                <tr class="text-center">
                    <th>Nama</th>
                    <th class="border-start">Total Anggota</th>
                    <th class="border-start">Paket</th>
                    <th class="border-start">Level</th>
                    <th class="border-start">Status</th>
                </tr>
            </thead>
            <tbody class="small">
                {{ $rows }}
            </tbody>
        </table>
    </div>
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/table-structure.css?t=' . mt_rand(1000001, 9999999)) }}">
@endpush

@push('scripts')
    <script src="{{ asset('js/table-structure.js') }}"></script>
@endpush
