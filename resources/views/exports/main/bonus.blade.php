<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <style>
        table{
            width: 100%;
            border-collapse: collapse;
        }
        td {
            border: 1px solid #000000 !important;
        }
        th,td {
          
            text-align: left;
            vertical-align: top
        }
    </style>
</head>
<body>

    {{-- <table >
        <thead >
        <tr >
            <th><b>Dari</b></th>
            <th>{{$dari}}</th>
            
        </tr>
        <tr>
            <th><b>Sampai</b></th>
            <th>{{$sampai}}</th>
        </tr>
        <tr>

        </tr>
    </table> --}}

    <table >
        <thead >
        <tr >
            <th align="center" style=" border: 1px solid #000000;"><b>Tanggal</b></th>
            <th align="center" style=" border: 1px solid #000000;"><b>Kode</b></th>
            <th align="center" style=" border: 1px solid #000000;"><b>Member</b></th>
            <th align="center" style=" border: 1px solid #000000;"><b>Bank</b></th>
            <th align="center" style=" border: 1px solid #000000;"><b>Tipe Bonus</b></th>
            <th align="center" style=" border: 1px solid #000000;"><b>Total Bonus</b></th>
            <th align="center" style=" border: 1px solid #000000;"><b>Admin Fee</b></th>
            <th align="center" style=" border: 1px solid #000000;"><b>Admin Transfer</b></th>
        </tr>
        </thead>
        <tbody >
            {{-- <tr >
                <td width="20" style=" border: 1px solid #000000; word-wrap: break-word"></td>
                <td width="20" style=" border: 1px solid #000000; word-wrap: break-word" ></td>
                <td width="20" style=" border: 1px solid #000000; word-wrap: break-word" ></td>
                <td width="20" style=" border: 1px solid #000000; word-wrap: break-word" >{{ $totalJ}}</td>
                <td width="20" style=" border: 1px solid #000000; word-wrap: break-word" >{{ $totalV }}</td>
                <td width="20" style=" border: 1px solid #000000; word-wrap: break-word" >{{ $totalB }}</td>
                <td width="20" style=" border: 1px solid #000000; word-wrap: break-word" ></td>
                
            </tr> --}}
        @foreach($dataList as $item)
            <tr >
                <td width="20" valign="top" style=" border: 1px solid #000000; word-wrap: break-word">{{ $item->wd_date }}</td>
                <td width="20" valign="top" style=" border: 1px solid #000000; word-wrap: break-word" >{{ $item->wd_code }}</td>
                <td width="20" valign="top" style=" border: 1px solid #000000; word-wrap: break-word" >
                    {{$item->user->name}} <br>
                    {{$item->user->username}} - {{$item->user->phone}}
                </td>
                <td width="20" valign="top" style=" border: 1px solid #000000; word-wrap: break-word" >
                    {{ $item->bank_code }} <br>
                    {{$item->bank_acc_no}} <br>
                    {{$item->bank_acc_name}}
                </td>
                <td width="20" valign="top" style=" border: 1px solid #000000; word-wrap: break-word" >
                    {{Arr::get(BONUS_MITRA_NAMES, $item->wd_bonus_type, '-')}}
                </td>
                <td width="20" valign="top" style=" border: 1px solid #000000; word-wrap: break-word" >{{ $item->total_bonus }}</td>
                <td width="20" valign="top" style=" border: 1px solid #000000; word-wrap: break-word" >
                    @php
                         if($item->wd_bonus_type == BONUS_MITRA_PRESTASI){
                            $fee = 0;
                        }else{
                            $fee = $item->fee;
                        }
                    @endphp
                    {{ $fee }}
                </td>
                <td width="20" valign="top" style=" border: 1px solid #000000; word-wrap: break-word" >
                    @php
                        if($item->wd_bonus_type == BONUS_MITRA_PRESTASI){
                            $transfer = $item->total_bonus;
                        }else{
                            $transfer = $item->total_transfer;
                        }
                    @endphp 

                    {{ $transfer }}
                </td>
              
                
            </tr>
        @endforeach
        </tbody>
    </table>
    
</body>
</html> 