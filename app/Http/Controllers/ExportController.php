<?php

namespace App\Http\Controllers;

use App\Exports\witdrawBonusAllExport;
use App\Exports\witdrawBonusPrestasiExport;
use App\Exports\witdrawBonusSponsorExport;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ExportController extends Controller
{


    public function exportBonusSponsor(Request $request)
    {
        $start_date = resolveTranslatedDate($request->startDate ?: date('j F Y'), ' ');
        $end_date = resolveTranslatedDate($request->endDate ?: date('j F Y'), ' ');
        $startDate = Carbon::createFromTimestamp(strtotime($start_date));
        $endDate = Carbon::createFromTimestamp(strtotime($end_date));
        $formatStart = $startDate->format('Y-m-d');
        $formatEnd = $endDate->format('Y-m-d');

        $bonusType = [BONUS_MITRA_SPONSOR,BONUS_MITRA_RO];

        if (empty($status)) $status = CLAIM_STATUS_PENDING;

       

        return Excel::download(new witdrawBonusSponsorExport(['status'=> $status, 'bonusType'=> $bonusType, 'startDate' => $formatStart,'endDate' => $formatEnd]), 'bonus-sponsor.xlsx');

        // dd($request);
    }

    public function exportBonusPrestasi(Request $request)
    {
        $start_date = resolveTranslatedDate($request->startDate ?: date('j F Y'), ' ');
        $end_date = resolveTranslatedDate($request->endDate ?: date('j F Y'), ' ');
        $startDate = Carbon::createFromTimestamp(strtotime($start_date));
        $endDate = Carbon::createFromTimestamp(strtotime($end_date));
        $formatStart = $startDate->format('Y-m-d');
        $formatEnd = $endDate->format('Y-m-d');

        $bonusType = BONUS_MITRA_PRESTASI;

        if (empty($status)) $status = CLAIM_STATUS_PENDING;

       

        return Excel::download(new witdrawBonusPrestasiExport(['status'=> $status, 'bonusType'=> $bonusType, 'startDate' => $formatStart,'endDate' => $formatEnd]), 'bonus-prestasi.xlsx');

        // dd($request);
    }

    public function exportBonusAll(Request $request)
    {
        $start_date = resolveTranslatedDate($request->startDate ?: date('j F Y'), ' ');
        $end_date = resolveTranslatedDate($request->endDate ?: date('j F Y'), ' ');
        $startDate = Carbon::createFromTimestamp(strtotime($start_date));
        $endDate = Carbon::createFromTimestamp(strtotime($end_date));
        $formatStart = $startDate->format('Y-m-d');
        $formatEnd = $endDate->format('Y-m-d');

        $bonusType = [BONUS_MITRA_PRESTASI,BONUS_MITRA_RO,BONUS_MITRA_SPONSOR];

        if (empty($status)) $status = CLAIM_STATUS_PENDING;

       

        return Excel::download(new witdrawBonusAllExport(['status'=> $status, 'bonusType'=> $bonusType, 'startDate' => $formatStart,'endDate' => $formatEnd]), 'bonus-all.xlsx');

        // dd($request);
    }

    public function exportBonusAgentLevel(Request $request)
    {
        $start_date = resolveTranslatedDate($request->startDate ?: date('j F Y'), ' ');
        $end_date = resolveTranslatedDate($request->endDate ?: date('j F Y'), ' ');
        $startDate = Carbon::createFromTimestamp(strtotime($start_date));
        $endDate = Carbon::createFromTimestamp(strtotime($end_date));
        $formatStart = $startDate->format('Y-m-d');
        $formatEnd = $endDate->format('Y-m-d');

        $bonusType = BONUS_MITRA_AGENT;

        if (empty($status)) $status = CLAIM_STATUS_PENDING;

       

        return Excel::download(new witdrawBonusPrestasiExport(['status'=> $status, 'bonusType'=> $bonusType, 'startDate' => $formatStart,'endDate' => $formatEnd]), 'bonus-agent-level.xlsx');

        // dd($request);
    }
}
