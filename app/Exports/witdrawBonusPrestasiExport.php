<?php

namespace App\Exports;

use App\Models\UserWithdraw;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class witdrawBonusPrestasiExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $data;

    function __construct($data) {
        $this->data = $data;
       

        // dd($this->data->bonusType);
    }

    public function view(): View
    {
        

        $dataList = UserWithdraw::with('user')->where('status',$this->data['status'])->where('wd_bonus_type',$this->data['bonusType'])->whereBetween('wd_date',[$this->data['startDate'],$this->data['endDate']])->orderBy('wd_date','desc')->get(); 

        // dd($dataList);

      

        return view('exports.main.bonus', [
            'dataList' => $dataList
        ]);
    }
}
